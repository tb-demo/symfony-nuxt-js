# Symfony Api demo project using Api Platform package and nuxtJs as frontend

  - [Installation Guide](Installation.md)
  - [Api documentation and test api with swagger ui](ApiUI.md)
  - [Nuxt Frontend Demo video](NuxtJs-Frontend-demo.mp4)

<video width="720" height="405" controls>
<source src="NuxtJs-Frontend-demo.mp4" type="video/mp4">
</video>

